﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicalChaosMonkey : MonoBehaviour {

	public bool isEnabled = true;

	[RangeAttribute(0.0f, 1.0f), Tooltip("Chance of simulating a dropped audio frame to desync audio.")]
	public float chaos = 0.1f;
	private bool isDroppedFrame;

	AudioSource audioTarget;

	// Use this for initialization
	void Start () {
		audioTarget = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		#if DEBUG
		if (isEnabled) {
			isDroppedFrame = Random.value < chaos ? true : false;

			if (isDroppedFrame) audioTarget.Pause();
			else audioTarget.UnPause();
		}
		#endif
	}
}
