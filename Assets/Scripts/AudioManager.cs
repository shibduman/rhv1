﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	private static AudioManager _instance;
	public static AudioManager instance { get; private set; }

	public int maxVoices = 20;
	private List<AudioSource> sfx;
	public float pitchModulation = 0.05f;

	void Start()
	{
		if (_instance == null) _instance = this;
		else Destroy(this);
		instance = _instance;

		sfx = new List<AudioSource>();
		sfx.Capacity = 20;

		//Create the components to be added to the object as the sfx, max voices = max simultaneous clips
		for (int i = 0; i < maxVoices; i++)
		{
			AudioSource sound = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
			sound.playOnAwake = false;
			sound.loop = false;
			sfx.Add(sound);
		}
	}

	/// Finds the first unused audio source and uses that to play the goddamn clip specified
	// returns false if it can't play a clip because you used too many voices at once you goddammn retard
	public bool PlaySound(AudioClip clip, bool randomize = false, float volume = 1.0f)
	{
		foreach (AudioSource voice in sfx)
		{
			if (voice.isPlaying) continue;

			//so not a busy audiosource
			voice.clip = clip;
			if (randomize) voice.pitch = Random.Range(1.0f - pitchModulation, 1.0f + pitchModulation);
			voice.volume = volume;
			voice.Play();
			return true;
		}
		return false;
	}

	public void StopAllSounds()
	{
		foreach (AudioSource voice in sfx)
		{
			voice.Stop();
		}
	}
}
