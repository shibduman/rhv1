﻿using System; //for string format in debug
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

	// Use this for initialization
	private Rigidbody2D rbody;
	private Animator c_animator;
	private Conductor conductor;

	//Debug
	public Text velocityText;

	//Music
	public Sequencer sequencer;
	private bool importantNote;

	//Player states
	public bool isGrounded = false;
	public bool isWallClinging = false;
    private bool leftInput = false;
    private bool rightInput = false;
    private bool holdingUp = false;

    //Player toggles
    public bool enableWallJump = false;
    public int cancelParticleAmount = 50;

	//dashing
	private bool isDashing;
	private bool isSuperDashing = false;
	private bool canDash;
	public float dashDuration = 0.25f;
	public float dashCooldown = 0.5f;
	public float dashSpeedMultiplier = 5.0f;
	public float dashNoteMultiplier = 10.0f;
	public GameObject afterImageObject;
	private float elapsedDashCD = 0.5f;

	//attacking
	private bool canAttack;
	public float attackCooldown = 0.1f;
	public float comboWindow = 10.0f;

    //attack collider configurations
    public string groundAttackAnimationString;
    public GameObject groundAttackColliderObject;
    public float groundAttackDuration;
    private BoxCollider2D[] groundAttackColliders;

    public string groundUpAttackAnimationString;
    public GameObject groundUpAttackColliderObject;
    public float groundUpAttackDuration;
    private BoxCollider2D[] groundUpAttackColliders;

    public string airAttackAnimationString;
    public GameObject airAttackColliderObject;
    public float airAttackDuration;
    private BoxCollider2D[] airAttackColliders;

    public string airUpAttackAnimationString;
    public GameObject airUpAttackColliderObject;
    public float airUpAttackDuration;
    private BoxCollider2D[] airUpAttackColliders;


    //attack state tracking
	private float elapsedAttackCD = 0.5f;
    private float elapsedAttackTime = 0;
	private string[] attackSets;
    private bool isAttacking = false;
    private bool canCancel = false;
    private ParticleSystem playerCancelParticles;
    private bool isInCancelWindow = false;
    private BoxCollider2D[] currentAttackColliders;
    private int currentAttackColliderIndex;
    private float lastColliderActivationTime = 0;
    private float currentColliderDuration;
    private BoxCollider2D currentAttackFrame = null;

	//wall cling max slide speed
	public float wallClingSpeedCap = -5.0f;
	public float wallKickMultiplier = 40.0f;

	public float moveSpeed;
	public float jumpHeight;
	public float accelerationValue = 2.0f;
	public float speedCap = 20.0f;

    //fast falling paramters
	public float fastFallSpeed = -125.0f;
    public float fastFallTapSpeed = 0.25f;
    private float fastFallTimer = 0.0f;
    private float currentVerticalInput;
    private bool fastFallWindow = false;
    private bool jumpPressed;
    private bool secondTap = false;

    //flinching
    public float flinchDuration = 0.75f;
    public float elapsedFlinch = 0.0f;
    public float knockbackSpeed = 100.0f;
    public bool isFlinching = false;

    private Health health;

    //end trigger stuff
    private VictoryTrigger v_trigger;


    void Start()
    {
        rbody = GetComponent<Rigidbody2D>();
        c_animator = GetComponent<Animator>();
        conductor = Conductor.instance;
        sequencer = conductor.sequencer;

        //fetch the colliders from the objects that contain them
        //make sure that all the object they pull from are tagged
        //differently from the player
        groundAttackColliders = groundAttackColliderObject.GetComponents<BoxCollider2D>();
        groundUpAttackColliders = groundUpAttackColliderObject.GetComponents<BoxCollider2D>();
        airAttackColliders = airAttackColliderObject.GetComponents<BoxCollider2D>();
        airUpAttackColliders = airUpAttackColliderObject.GetComponents<BoxCollider2D>();


        //particles are initialzed here to communicate an attack
        //was hit on beat and the subsequent one can be cancelled
        //@TODO: Need to add in a player flash or something to compliment the particles.
        playerCancelParticles = GameObject.Find("CancelParticles").GetComponent<ParticleSystem>();

        isWallClinging = false;

        // sequencer.importantNoteHit += ToggleImportantNote;
        // sequencer.nonImportantNote += ToggleOffImportantNote;

        v_trigger = GameObject.FindGameObjectWithTag("VictoryTrigger").GetComponent<VictoryTrigger>();
        v_trigger.victory += Victory;

        health = GetComponent<Health>();
        health.Damage += Flinch;
        health.Die += Die;
    }

    // Update is called once per frame
    void Update()
    {

        if (health.isDead) return; //bitch you dead, ain't nobody moving when they're dead

        //capture a few values
        //@TODO: currently importantnote is just being fetched from the
        //sequencer, maybe fix the delegate?
        jumpPressed = Input.GetButton("Jump");
        currentVerticalInput = Input.GetAxisRaw("Vertical");
        rightInput = Input.GetAxisRaw("Horizontal") > 0.0f;
        leftInput = Input.GetAxisRaw("Horizontal") < 0.0f;
        importantNote = sequencer.isImportantNoteHit;
        holdingUp = currentVerticalInput > 0;
        isFlinching = c_animator.GetBool("isFlinching");

        // Debug.Log("important note is " + importantNote);

        //flinching
        if (isFlinching)
        {
            elapsedFlinch += Time.deltaTime;
            if (elapsedFlinch > flinchDuration) SetFlinching(false);
            else return; //If stuck in a flinch you can't do shit
        }

        Vector2 direction = new Vector2(rbody.velocity.x, rbody.velocity.y);

        //Directional Input
        if (rightInput)
        {
            direction.x = direction.x + (accelerationValue * Time.deltaTime);
            transform.localScale = new Vector3(-1 * Math.Abs(transform.localScale.x), transform.localScale.y, 1);
            if (isGrounded)
            {
                c_animator.SetTrigger("playerRun");
            }
        }
        else if (leftInput)
        {
            direction.x = direction.x + -(accelerationValue * Time.deltaTime);
            transform.localScale = new Vector3(Math.Abs(transform.localScale.x), transform.localScale.y, 1);
            if (isGrounded)
            {
                c_animator.SetTrigger("playerRun");
            }
        }
        else if (isGrounded)
        {
            c_animator.SetTrigger("playerIdle");
        }

        //window to control double tap timing for fast falls
        if (fastFallTimer > 0)
        {
            fastFallTimer -= Time.deltaTime;
        }
        else
        {
            fastFallTimer = 0;
            fastFallWindow = false;
            secondTap = false;
        }

        direction.x = Mathf.Clamp(direction.x, -speedCap, speedCap);

        //check if player has double tapped in the necessary time
        if (currentVerticalInput < 0.0f && !isGrounded && !jumpPressed)
        {
            if (fastFallWindow && secondTap)
            {
                //apply fast fall
                direction.y = fastFallSpeed;
                secondTap = false;
            }
            else
            {
                fastFallTimer = fastFallTapSpeed;
                fastFallWindow = true;
            }
        }
        else if (currentVerticalInput >= 0.0f && fastFallWindow)

        {
            secondTap = true;
        }

        rbody.velocity = direction; //Warning: If moving into a wall with friction on the physics material this will cause the player to stop!

        //Dashing
        elapsedDashCD += Time.deltaTime;
        if (elapsedDashCD > dashDuration) isDashing = false;
        if (elapsedDashCD > dashCooldown) canDash = true;

        if (Input.GetButtonDown("Dash") && (canDash || importantNote))
        {
            elapsedDashCD = 0.0f;
            isDashing = true;
            canDash = false;
            //isSuperDashing = Conductor.currentNote == NoteHits.quarterHit ? true : false;
            //isSuperDashing = conductor.isNoteActive(NoteHits.quarterHit) & importantNote;
            isSuperDashing = sequencer.isImportantNoteHit;

            if (isSuperDashing)
            {
                //@TODO: Fix super dashing so it doesn't fuck with player momentum,
                //this isn't super urgent as dashing isn't something the player starts with
                Transform targetImage = afterImageObject.transform.Find(isGrounded ? "GroundAfterImage" : "AirAfterImage");
                targetImage.gameObject.SetActive(true);
                targetImage.transform.position = transform.position;
                targetImage.localScale = new Vector3(transform.localScale.x < 0 ? Math.Abs(targetImage.localScale.x) * -1 : Math.Abs(targetImage.localScale.x), targetImage.localScale.y, targetImage.localScale.z);
            }
            #if DEBUG
            if (isSuperDashing) Debug.Log("Super dash!");
            else Debug.Log("Dash");
            #endif
        }

        if (isDashing)
        {
            Vector2 vel = new Vector2(rbody.velocity.x * dashSpeedMultiplier, rbody.velocity.y);
            if (isSuperDashing)
            {
                vel.x *= dashNoteMultiplier;
            }
            rbody.velocity = vel;
        }

        //Wall ClingingClinging
        if (isWallClinging)
        {
            Vector2 velocity = new Vector2(rbody.velocity.x, (wallClingSpeedCap > rbody.velocity.y ? wallClingSpeedCap : rbody.velocity.y));
            rbody.velocity = velocity;
        }

        //Jumping
        if (jumpPressed)
        {
            if (isGrounded)
            {
                rbody.velocity = new Vector2(rbody.velocity.x, jumpHeight);
            }
            //@TODO: Wall jumping needs to be reworked for later in the game,
            //it's also not an initial mechanic at the moment.
            else if (isWallClinging && enableWallJump)
            {
                //wall jump
                // Debug.Log("wall jump");
                int pushDirection = Input.GetAxisRaw("Horizontal") < 0.0f ? -1 : 1;
                rbody.velocity = new Vector2(rbody.velocity.x + pushDirection * wallKickMultiplier, jumpHeight);
            }
            isGrounded = false;
        }

        if (isGrounded == false)
        {
            c_animator.ResetTrigger("playerLand");
            c_animator.ResetTrigger("playerRun");
            c_animator.SetTrigger("playerJump");
        }

        elapsedAttackCD += Time.deltaTime;
        if (elapsedAttackCD > attackCooldown || (canCancel && isInCancelWindow)) canAttack = true;

        if (Input.GetButtonDown("Attack") && canAttack)
        {
            elapsedAttackCD = 0.0f;
            canAttack = false;

            if (isGrounded && holdingUp)
            {
                LaunchAttack(groundUpAttackAnimationString, groundUpAttackColliders, groundUpAttackDuration);
            }
            else if (isGrounded)
            {
                LaunchAttack(groundAttackAnimationString, groundAttackColliders, groundAttackDuration);
            }
            else if (holdingUp)
            {
                LaunchAttack(airUpAttackAnimationString, airUpAttackColliders, airUpAttackDuration);
            }
            else
            {
                LaunchAttack(airAttackAnimationString, airAttackColliders, airAttackDuration);
            }

            //@TODO: change cancel so that there's a time limit on
            //how long of a window the player can cancel in
            //and there should still be a window that the player
            //has to sit through to be able to cancel into the next attack,
            //maybe just subtract some time off the next attack?
            canCancel = importantNote;
            if (canCancel)
            {
                playerCancelParticles.Emit(cancelParticleAmount);
                playerCancelParticles.Stop();
                Debug.Log("player can cancel, emitting particles");
            }
        }

        //attack processing if player is currently attacking
        //@TODO: add player attack specific time controls
        if (isAttacking)
        {
            lastColliderActivationTime += Time.deltaTime;
            // if (currentAttackColliderIndex == 0)
            // {
            //     currentAttackColliders[0].enabled = true;
            //     currentAttackColliderIndex++;
            // }
			//trigger attack colliders if enough time has passed
			if (lastColliderActivationTime >= currentColliderDuration) {
                if (currentAttackColliderIndex < currentAttackColliders.Length && currentAttackColliderIndex != -1)
                {
                    currentAttackColliders[currentAttackColliderIndex - 1].enabled = false;
                    currentAttackColliders[currentAttackColliderIndex].enabled = true;
                    currentAttackColliderIndex++;
                }
                else if (currentAttackColliderIndex == currentAttackColliders.Length - 1)
                {
                    currentAttackColliders[currentAttackColliderIndex - 1].enabled = false;
                    isAttacking = false;
                    currentAttackColliderIndex = 0;
                }
            }
			lastColliderActivationTime = 0;
            elapsedAttackTime += Time.deltaTime;
		}

        c_animator.SetBool("isDashing", isDashing);
        c_animator.SetBool("isWallClinging", isWallClinging);

        //Debug
        // #if DEBUG
        // velocityText.text = string.Format("< {0:00.0f}, {1:00.0f} >", rbody.velocity.x, rbody.velocity.y);
        // #endif
    }

    void ToggleOffImportantNote()
    {
        Debug.Log("no important note");
        importantNote = false;
    }

    void ToggleImportantNote()
    {
        importantNote = true;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("GroundMesh"))
        {
            isGrounded = true;
            c_animator.ResetTrigger("playerJump");
            c_animator.SetTrigger("playerLand");
        }

        if (other.gameObject.CompareTag("WallMesh"))
        {
            if (!isGrounded && (Input.GetAxisRaw("Horizontal") != 0.0f)) isWallClinging = true;
            else isWallClinging = false;
        }
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("GroundMesh"))
        {
            isGrounded = true;
            c_animator.ResetTrigger("playerJump");
            c_animator.SetTrigger("playerLand");
        }
        else if (other.gameObject.CompareTag("WallMesh"))
        {
            if (!isGrounded && (Input.GetAxisRaw("Horizontal") != 0.0f)) isWallClinging = true;
            else isWallClinging = false;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("WallMesh")) isWallClinging = false;
        if (other.gameObject.CompareTag("GroundMesh")) isGrounded = false;
    }

    void Victory()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        rbody.velocity = new Vector2(0, 0);
        rbody.gravityScale = 0;
        enabled = false;
    }

    void Flinch() //from taking damage
    {
        SetFlinching(true);
        rbody.velocity = Vector3.zero;
        elapsedFlinch = 0.0f;
    }

    void Die()
    {
        SetFlinching(false);
        c_animator.SetTrigger("playerDie");
    }

    public void Knockback(Vector3 direction)
    {
        rbody.velocity = direction.normalized * knockbackSpeed;
    }

    public void ChangeCancel (int inWindow)
    {
        isInCancelWindow = inWindow != 0;
    }

    /*
    This function is called using animation events to set a specific collider as active.
    Basically you use an animation event to pass in an index of a collider that gets
    set in the LaunchAttack function based on what the player is doing.
    Passing in -1 basically ends the attack.
    The colliders are held in child game objects and are tagged as "attack".

    @TODO: Right now, multiple hits are possible as colliders get enabled/disabled.
    We probably should have a way to set an attack as single hit or multi hit or something
    so that enemies don't take 3 points of damage or something from a single swipe.
    */
    public void ToggleAttackFrame(int frame)
    {
        switch (frame)
        {
            case 0:
                currentAttackFrame = currentAttackColliders[0];
                currentAttackFrame.enabled = true;
                break;
            case -1:
                currentAttackFrame.enabled = false;
                break;
            default:
                currentAttackFrame.enabled = false;
                currentAttackFrame = currentAttackColliders[frame];
                currentAttackFrame.enabled = true;
                break;
        }
    }

    private void SetFlinching(bool flinch)
    {
        c_animator.SetBool("isFlinching", flinch);
        isFlinching = flinch;
    }

    //@TODO: Not sure if some of the stuff here is needed anymore
    //and some of the variables should just be removed.
    private void LaunchAttack (String attackAnimation, BoxCollider2D[] colliderSet, float duration)
    {
        currentAttackColliders = colliderSet;
        elapsedAttackTime = 0;

        c_animator.StopPlayback();
        c_animator.SetTrigger(attackAnimation);
        currentColliderDuration = duration / currentAttackColliders.Length;
        isAttacking = true;
    }
}
