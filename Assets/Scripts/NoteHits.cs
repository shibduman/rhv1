﻿using UnityEngine;

public enum NoteHits
{
	quarterHit,
	firstSixteenthHit,
	eighthHit,
	secondSixteenthHit,
	noHit
}
