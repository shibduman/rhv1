﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour {

	public Health health;
	public bool enablePlayerCollisionDamage;
	private SpriteRenderer sprite;
	private Collider2D characterCollider;

	void Start()
	{
		health = GetComponent<Health>();
		sprite = GetComponent<SpriteRenderer>();
		characterCollider = GetComponent<Collider2D>();

		health.Damage += DamageFX;
		health.Die += OnDeath;
	}

	//the below collision methods contain logic for dealing
	//with what happens when a player collides with an enemy
	//when the enemy touches the player it should do damage,
	//but it shouldn't cause repeat instances of damage
	//until the player is done flinching, so we'll
	//shut off the colliders until the player stands back up
	//or leaves the collider box of the enemy
	void OnCollisionEnter2D(Collision2D other)
	{
		var otherObject = other.gameObject;
		Debug.Log("other object is " + (otherObject.CompareTag("Attack") ? "Attack" : "unknown"));
		if (otherObject.CompareTag("Attack"))
		{
			health.OnDamage(1, gameObject);

		} else if (enablePlayerCollisionDamage &&
			otherObject.CompareTag("Player"))
		{
			var playerComponent = otherObject.GetComponent<PlayerController>();
			var flinching = playerComponent.isFlinching;
			if (!flinching)
			{
			    //do damage to player
				otherObject.GetComponent<Health>().OnDamage(1, gameObject);
			}

		}
	}


	void OnCollisionStay2D(Collision2D other)
	{
		var otherObject = other.gameObject;
		if (otherObject.CompareTag("Player"))
		{
			var flinching = otherObject.GetComponent<PlayerController>().isFlinching;
			var playerCollider = otherObject.GetComponent<Collider2D>();
			if (flinching)
			{
				Physics2D.IgnoreCollision(playerCollider, characterCollider, true);
			}
			else
			{
				Physics2D.IgnoreCollision(playerCollider, characterCollider, false);
			}
		}
	}

	void OnCollisionExit2D(Collision2D other)
	{
		var otherObject = other.gameObject;
		if (otherObject.CompareTag("Player"))
		{
			var playerCollider = otherObject.GetComponent<Collider2D>();
			Physics2D.IgnoreCollision(playerCollider, characterCollider, false);
		}
	}

	public void DoDamage(int amount) {
		health.OnDamage(amount, gameObject);
	}

	private void DamageFX()
	{
		sprite.color = Color.red;
	}

	private void OnDeath()
	{
		gameObject.SetActive(false);
	}
}
