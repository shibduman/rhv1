﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitStun : MonoBehaviour {

	public float hitStunTime = 0.0f;

	private Animator e_animator;
	private bool isStunned = false;
	private float stunTime = 0.0f;
	private float animationSpeed;

	void Start ()
	{
		e_animator = GetComponent<Animator>();
		GetComponent<Health>().Damage += OnHit;
	}

	private void Update()
	{
		if (isStunned)
		{
			stunTime += Time.deltaTime;
			if (stunTime > hitStunTime)
			{
				e_animator.speed = animationSpeed;
				isStunned = false;
				stunTime = 0.0f;
			}
		}
	}
	void OnHit()
	{
		if (hitStunTime > 0 && !isStunned)
		{
			isStunned = true;
			animationSpeed = e_animator.speed;
			e_animator.speed = 0;
		}
	}
}
