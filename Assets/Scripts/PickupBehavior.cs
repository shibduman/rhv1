﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupBehavior : MonoBehaviour {

	// Use this for initialization
	public float scaleTo;
	public float shrinkRate;
	public Sequencer seq;
	private float previousScale;

	//being annoying

	void Start () {
		previousScale = 0f;
	}

	// Update is called once per frame
	void Update () {
		if(seq.isImportantNoteHit && seq.strictHit) {
			transform.localScale = new Vector3(scaleTo,scaleTo,1.0f);
			previousScale = scaleTo;
		} else {
			previousScale = previousScale - shrinkRate;
			previousScale = previousScale > 3 ? previousScale : 3;
			transform.localScale = new Vector3(previousScale,previousScale,1.0f);
		}

		//being more annoying
		// switch (Conductor.currentNote)
		// {
		// 	case NoteHits.quarterHit:
		// 		sprite.color = Color.white;
		// 		break;
		// 	case NoteHits.eighthHit:
		// 		sprite.color = Color.black;
		// 		break;
		// 	case NoteHits.firstSixteenthHit:
		// 	case NoteHits.secondSixteenthHit:
		// 		sprite.color = Color.black;
		// 		break;
		// }
	}
}
