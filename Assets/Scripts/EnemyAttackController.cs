﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackController : MonoBehaviour
{
	public float moveRate = 20.0f;
	public float moveDuration = 10.0f;

	public Health health;
	public AudioClip hitNoise;
	public AudioClip deathNoise;
	public bool turnAroundOnHit = true;

	private float moveTimer;
	private int directionValue = 1;
	// Use this for initialization
	void Start ()
	{
		moveTimer = 0;
		health = gameObject.GetComponent<Health>();
		health.Damage += PlayHitSFX;
	}

	// Update is called once per frame
	void Update ()
	{
		if(moveTimer >= moveDuration)
		{
			ChangeDirection();
		}
		transform.position = new Vector3(transform.position.x + (directionValue * moveRate * Time.deltaTime), transform.position.y, transform.position.z);
		moveTimer += Time.deltaTime;
		//@TODO: currently this enemy will just straight up walk off an edge,
		//so some raycasting logic should be added to prevent it
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		var otherObject = other.gameObject;
		//if we want to turn around when we hit a player
		//make sure we actually hit them and they aren't flinching
		if (turnAroundOnHit &&
			otherObject.CompareTag("Player"))
		{
			var flinching = otherObject.GetComponent<PlayerController>().isFlinching;
			if (!flinching)
			{
				ChangeDirection();
			}
		}
	}

	void PlayHitSFX()
	{
		if (health.hp > 0) AudioManager.instance.PlaySound(hitNoise);
		else AudioManager.instance.PlaySound(deathNoise);
	}

	void ChangeDirection()
	{
		moveTimer = 0;
		directionValue = -directionValue;
	}
}
