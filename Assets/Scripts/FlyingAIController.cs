using System; //for string format in debug
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingAIController : MonoBehaviour
{

	public float moveRate = 20.0f;
	public float moveRange = 10.0f;
	public float attackTriggerRange = 10.0f;
	public float attackDelay = 1.0f;
	public GameObject player;
	public GameObject bulletPrefab;
	public bool enableAttacking;


	private float initialLocation;
	private int directionValue = 1;
	private bool attacking;
	private Animator e_animator;
	private float lastAttack;
	private int spriteOrientation = -1;
	private bool repositioning;

	public AudioClip hitNoise;
	public AudioClip deathNoise;
	public Health health;

	// Use this for initialization
	void Start ()
	{
		e_animator = GetComponent<Animator>();
		e_animator.SetTrigger("isIdle");
		initialLocation = transform.position.x;
		attacking = false;
		lastAttack = 0.1f;
		Patrol(false);

		health = gameObject.GetComponent<Health>();
		health.Damage += PlayHitSFX;

	}

	// Update is called once per frame
	void Update ()
	{

		//player is in range, and we can attack
		// Debug.Log(PlayerInRange());
		if (PlayerInRange() && !attacking && enableAttacking)
		{
			AttackPlayer();
		}
		//player is in range, and we're attacking
		else if (PlayerInRange() && attacking)
		{
			lastAttack += Time.deltaTime;
			//attack is over
			if (lastAttack > attackDelay)
			{
				attacking = false;
				lastAttack = 0.0f;
			}
			//attack is still happening
			else
			{
				//we've just attacked, reposition
				if (!repositioning)
				{

				}
			}
		}
		else if (!attacking)
		{
			if (lastAttack == 0.1f)
			{
				Patrol(false);
			}
			else
			{
				Patrol(true);
			}
		}
	}

	void Patrol(bool resetPosition)
	{
		e_animator.SetTrigger("isIdle");
		if (resetPosition)
		{
			initialLocation = transform.position.x;
		}
		if(Mathf.Abs(initialLocation - transform.position.x) > moveRange)
		{
			directionValue = -directionValue;
			spriteOrientation = -spriteOrientation;
		}
		// transform.Translate(new Vector3(directionValue * moveRate * Time.deltaTime, 0, 0), Space.Self);
		transform.position = new Vector3(transform.position.x + (directionValue * moveRate * Time.deltaTime), transform.position.y, transform.position.z);
		transform.localScale = new Vector3(spriteOrientation * Math.Abs(transform.localScale.x), transform.localScale.y, 1);

	}

	void AttackPlayer()
	{
		/*
		this triggers the attack animation for the flying enemy
		the SpawnProjectile method is called via an animation event on the flying enemy
		*/
		e_animator.SetTrigger("isAttacking");
	}

	void SpawnProjectile()
	{
		GameObject bullet;
		Transform copyTransform = transform;
		bullet = (GameObject)Instantiate(bulletPrefab);
		bullet.transform.position = transform.position;
	}


	bool PlayerInRange()
	{
		return Vector2.Distance(player.transform.position, this.transform.position) < attackTriggerRange ? true : false;
	}

	void Reposition()
	{
		repositioning = true;
	}

	void PlayHitSFX()
	{
		if (health.hp > 0) AudioManager.instance.PlaySound(hitNoise);
		else AudioManager.instance.PlaySound(deathNoise);
	}
}
