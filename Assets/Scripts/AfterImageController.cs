﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfterImageController : MonoBehaviour {

	public float timeDuration = 5.0f;

	private float currentDuration = 0;

	void Awake()
	{
		gameObject.SetActive(false);
	}

	// Update is called once per frame
	void Update () {
		if(currentDuration >= timeDuration)
		{
			currentDuration = 0;
			gameObject.SetActive(false);
		}
		else
		{
			currentDuration += Time.deltaTime;
		}
	}

	public void StartAfterImage (Vector3 pos)
	{
		transform.parent.position = pos;
	}

}
