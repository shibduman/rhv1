﻿using UnityEngine;

public class Health : MonoBehaviour {

	public int maxHp;
	public int hp { get; private set; }
	public bool isDead = false;

	public delegate void EventDelegate();
	public EventDelegate Damage;
	public EventDelegate Die;

	void Start ()
	{
		hp = maxHp;
	}


	public void OnDamage(int damage, GameObject other = null)
	{
		if (isDead) return;

		hp -= damage;
		if (Damage != null) Damage();

		#if DEBUG
		Debug.Log(gameObject.name + " took " + damage + " damage from " + (other == null ? "anonymous source." : other.name));
		#endif

		if (hp <= 0)
		{
			if (Die != null) Die();
			isDead = true;

			#if DEBUG
			Debug.Log(gameObject.name + " has died.");
			#endif
		}
	}
}
