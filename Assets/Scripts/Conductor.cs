﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Sequencer))]
public class Conductor : MonoBehaviour
{
	//Make this a fucking singleton
	private static Conductor _instance;
	public static Conductor instance { get { return _instance; } }

	public float bpm;
	public float m_timeBuffer;
	public AudioSource track;
	public static NoteHits currentNote;

	public delegate void Step();
	public Step step;

	public float delay;
	public float crotchet;
	private float lastTick;
	private float lastTickStrict;
	private float nextTick;
	private float nextTickStrict;
	private int noteCounter;

	private float lastStepQuarter;
	private float lastStepSixteenth;
	private float lastStepEigth;
	private float lastStepFinal;

	public Sequencer sequencer { get; private set; }

	void Awake()
	{
		SetupSequencer();
	}

	void SetupSequencer()
	{
		//enforce singleton
		if (_instance != null && _instance != this) Destroy(this.gameObject);
		else _instance = this;
		sequencer = GetComponent<Sequencer>();
		sequencer.song.songTracks = GetComponents<AudioSource>();
		crotchet = 60 / (bpm * 4);
	}

	// Use this for initialization
	void Start()
	{
		SetupCounters();
		track = sequencer.song.songTracks[0];
		track.Play();
	}

	void SetupCounters()
	{
		lastTick = 0 + delay; //buffered
		nextTick = lastTick + crotchet - m_timeBuffer;
		lastTickStrict = 0 + delay; //unbuffered
		nextTickStrict = lastTickStrict + crotchet;
		noteCounter = 1;
	}

	void Update ()
	{
		if (track.time < delay) return;

		//Buffered conductor
		if (track.time >= nextTick)
		{
			//we've hit a note
			// Debug.Log("tick");
			lastTick += crotchet;
			nextTick = lastTick + crotchet - m_timeBuffer;
			//determine note type
			switch (noteCounter)
			{
				case 1:
					//quarter
					currentNote = NoteHits.quarterHit;
					break;
				case 2:
					//first 16th
					currentNote = NoteHits.firstSixteenthHit;
					break;
				case 3:
					//8th
					currentNote = NoteHits.eighthHit;
					break;
				case 4:
					//second 16th
					currentNote = NoteHits.secondSixteenthHit;
					noteCounter = 0;
					break;
			}
			noteCounter++;
		}
		else if (lastTick < track.time)
		{
			currentNote = NoteHits.noHit;
		}

		//Strict conductor
		if (track.time >= nextTickStrict)
		{
			lastTickStrict += crotchet;
			nextTickStrict = lastTickStrict + crotchet;
			if (step != null) step(); //call this on hit

			switch (currentNote)
			{
				case NoteHits.quarterHit:
					lastStepQuarter = track.time;
					break;
				case NoteHits.firstSixteenthHit:
					lastStepSixteenth = track.time;
					break;
				case NoteHits.eighthHit:
					lastStepEigth = track.time;
					break;
				case NoteHits.secondSixteenthHit:
					lastStepFinal = track.time;
					break;
			}
		}

		if (!track.isPlaying)
		{
			// int songIndex = sequencer.song.currentSongIndex == 0 ? 1:0;
			int songIndex = 0;
			sequencer.song.currentSongIndex = songIndex;
			track = sequencer.song.songTracks[songIndex];
			sequencer.ResetSequencer();
			SetupSequencer();
			SetupCounters();
            track.Play();
		}
	}

	public bool isNoteActive(NoteHits noteToTest)
	{
		float previousNote = 0.0f;
		switch (noteToTest)
		{
			case NoteHits.quarterHit:
				previousNote = lastStepQuarter;
				break;
			case NoteHits.firstSixteenthHit:
				previousNote = lastStepSixteenth;
				break;
			case NoteHits.eighthHit:
				previousNote = lastStepEigth;
				break;
			case NoteHits.secondSixteenthHit:
				previousNote = lastStepFinal;
				break;
		}

		float beatDuration = crotchet * 4; //crotchet is set to 16th notes atm, beat = 4 sixteenths
		if (track.time - previousNote > beatDuration / 2)
		{
			//The target note is upcoming
			float targetNote = previousNote + beatDuration;
			float error = Mathf.Abs(targetNote - track.time);
			Debug.Log("Timing error: " + error);
			return (error > m_timeBuffer) ? false : true;
		}
		else
		{
			//The target note was in the past
			float error = Mathf.Abs(previousNote - track.time);
			Debug.Log("Timing error: " + error);
			return (error > m_timeBuffer) ? false : true;
		}
	}
}
