﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryTrigger : MonoBehaviour
{

	public delegate void VictoryDelegate();
	public VictoryDelegate victory;

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			//victory
			if (victory != null)
			{
				victory();
			}
		}
	}

}
