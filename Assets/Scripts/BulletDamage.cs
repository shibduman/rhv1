﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDamage : MonoBehaviour
{
	public int damageAmount;
	public float bulletLifeTime;
	public float projectileSpeed;

	private Rigidbody2D rb;
	private Vector3 targetPosition;
	private float life;

	void Start()
	{
		life = 0.0f;
		targetPosition = GameObject.FindGameObjectsWithTag("Player")[0].transform.position;
		rb = GetComponent<Rigidbody2D>();
		rb.AddForce((targetPosition - transform.position).normalized * projectileSpeed);
	}
	// Update is called once per frame
	void Update ()
	{
		if (life > bulletLifeTime)
		{
			Destroy(gameObject);
		}
		else
		{
			life += Time.deltaTime;
		}
	}
	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.transform.tag == "Player")
		{
			Health playerHealth = other.gameObject.GetComponent<Health>();
			playerHealth.OnDamage(damageAmount, gameObject);
			other.gameObject.GetComponent<PlayerController>().Knockback(rb.velocity);
			Destroy(gameObject);
		}
	}
}
