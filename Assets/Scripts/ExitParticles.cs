﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitParticles : MonoBehaviour
{
    private ParticleSystem particles;
    private VictoryTrigger victory;
    // Use this for initialization
    void Start ()
    {
	particles = GetComponent<ParticleSystem>();
	particles.Stop();
	victory = GameObject.FindGameObjectWithTag("VictoryTrigger").GetComponent<VictoryTrigger>();
	victory.victory += BeginParticles;
	transform.position = victory.transform.position;
    }

    void BeginParticles()
    {
	particles.Play();
    }
}
