﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScript : MonoBehaviour
{
	public float moveRate = 20.0f;
	public float moveRange = 10.0f;

	private float initialLocation;
	private int directionValue = 1;

    // Use this for initialization
	void Start ()
	{
		initialLocation = transform.position.x;
	}

    // Update is called once per frame
	void FixedUpdate ()
	{
		if(Mathf.Abs(initialLocation - transform.position.x) > moveRange)
		{
			directionValue = -directionValue;
		}
	// transform.Translate(new Vector3(directionValue * moveRate * Time.deltaTime, 0, 0), Space.Self);
		transform.position = new Vector3(transform.position.x + (directionValue * moveRate * Time.deltaTime), transform.position.y, transform.position.z);
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		other.transform.SetParent(transform);
	}
	private void OnCollisionExit2D(Collision2D other)
	{
		other.transform.SetParent(null);
	}
}
