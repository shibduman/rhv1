﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BeatOMeterController : MonoBehaviour
{
	public Sequencer sequencer;
	public NoteHits noteToTrigger;
	public bool useImportantNotes;
	public float reductionRate;
	public float reductionLimit;

	private Image image;

	// Use this for initialization
	void Start ()
	{
		image = GetComponent<Image>();
		image.fillAmount = 0;
	}

	// Update is called once per frame
	void Update ()
	{
		if (noteToTrigger == Conductor.currentNote && noteToTrigger != NoteHits.noHit)
		{
			SetFill();
		}
		else if (sequencer.isImportantNoteHit && sequencer.strictHit && noteToTrigger == NoteHits.noHit)
		{
			// Debug.Log("important note for the beatometer" + sequencer.isImportantNoteHit);
			SetFill();
		}
		else if(image.fillAmount > reductionLimit)
		{
			image.fillAmount -= reductionRate;
		}
	}

	void SetFill()
	{
		image.fillAmount = 1;
	}
}
