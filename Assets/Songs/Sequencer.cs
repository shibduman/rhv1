
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.VersionControl;
using Newtonsoft.Json;

[RequireComponent(typeof(Conductor))]
public class Sequencer : MonoBehaviour
{
    public Conductor conductor;

    public TextAsset songJson;
    public Song song;

    private int measure;
    private List<float> importantTimes;
    public int step { get; private set; } //The current step in 16th notes

    public delegate void Trigger();
    public Trigger importantNoteHit;
    public Trigger nonImportantNote;

    public bool isImportantNoteHit { get; private set; }
    public bool strictHit {get; private set;}

    void Awake()
    {
        song = JsonConvert.DeserializeObject<Song>(songJson.text);
    }

    void Start()
    {
        // song = new Song();
        conductor.step += Step;
        conductor.delay = song.offset;
        importantNoteHit += TestHit;
        ResetSequencer();
    }
    //resets the fields
    public void ResetSequencer()
    {
        importantTimes = new List<float>();
        measure = 0;
        step = 0; //so initial step starts at 0 index (first quarter)
        // Debug.Log("crochet is " + conductor.crotchet);
        song.primary = song.songImportantNotes[song.currentSongIndex];
        for (int m_index = 0; m_index < song.primary.Length; m_index++)
        {
            int[] currentMeasure = song.primary[m_index];
            // Debug.Log("current measure is " + (m_index + 1));
            for (int m_step = 0; m_step < currentMeasure.Length; m_step++)
            {
                //* (m_step + 1) * (conductor.crotchet * 4)
                // Debug.Log((((m_index) * 16 + (currentMeasure[m_step] + 1)) * (conductor.crotchet)) + song.offset);
                importantTimes.Add((((m_index) * 16 + (currentMeasure[m_step] + 1)) * (conductor.crotchet)) + song.offset);
            }
        }
    }

    void Step()
    {
        step++;
        if (step > 16)
        {
            step = 1;
            measure++;
        }

        bool strictTiming = false;

        // Debug.Log("stepping at step " + step + " of measure "  + measure);
        for (int i = 0; i < song.primary[measure].Length; i++)
        {
            if (song.primary[measure][i] == step)
            {
                strictTiming = true;
                break;
            }
        }

        if (measure > song.primary.Length - 1 || importantTimes.Count == 0)
        {
            //we're outta important stuff to look for!
            NoteImportance(false);
        }
        else if (strictTiming)
        {

            NoteImportance(true, true);
        }
        else if (conductor.track.time >= importantTimes[0] - conductor.m_timeBuffer &&
                 conductor.track.time <= importantTimes[0] + conductor.m_timeBuffer)
        {

            NoteImportance(true);
        }
        else if (conductor.track.time > importantTimes[0] + conductor.m_timeBuffer)
        {
            importantTimes.RemoveAt(0);
            NoteImportance(false);
        }
        else
        {
            NoteImportance(false);
        }
    }

    private void NoteImportance(bool important, bool strict = false)
    {
        isImportantNoteHit = important;
        strictHit = strict;
        Trigger triggers = important ? importantNoteHit : nonImportantNote;

	//@TODO: fix this, it's only firing on strict hits,
	//maybe split up the delegates?
        if (triggers != null && strict)
        {
            triggers();
        }
    }

    private bool hasStep(int number)
    {
        if (number == step) return true;
        else return false;
    }

    private void TestHit()
    {
        // Debug.Log("Important note hit at { " + measure + " , " + step + "}");
    }
}
