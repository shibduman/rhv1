using System.Collections.Generic;
using UnityEngine;

public class Song
{
    public string title;
    public int bpm;
    public float offset;
    public int currentSongIndex;
    public int[][] primary;
    public AudioSource[] songTracks;
	public int[][][] songImportantNotes;
}