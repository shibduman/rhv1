using UnityEngine;

public class AnimationSong : Song
{
	public AnimationSong()
	{
		title = "Animation";
		bpm = 174;
		offset = 0.170f; //110ms delay before song starts

		primary = new int[][]
		{
			//data                                  //measure
			new int[] {},                           //1
			new int[] {},
			new int[] {},
			new int[] {},
			new int[] {},
			new int[] {},
			new int[] {},
			new int[] {},                           //8
			new int[] {0, 4, 6, 11, 14},            //9
			new int[] {4, 7, 10, 14},               //10
			new int[] {2, 4, 8, 12, 14},	        //11
			new int[] {0, 5, 9, 12},				//12
			new int[] {0, 4, 5, 9, 13},				//13
			new int[] {0, 5, 9}						//14
		};
	}
}